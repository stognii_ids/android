package com.facebookapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taras on 09.02.14.
 */
public class FriendsListActivity extends FragmentActivity {
    private ListView lvFriend;

    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        lvFriend = (ListView) findViewById(R.id.lvFriends);

        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // Get the user's data
            makeFriendsRequest(session);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        uiHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
    private void makeFriendsRequest(final Session session) {
        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMyFriendsRequest(session,
                new Request.GraphUserListCallback() {
                    @Override
                    public void onCompleted(List<GraphUser> users, Response response) {
                        ArrayList<FriendElement> elements = new ArrayList<FriendElement>();
                        for (int i = 0; i < users.size(); i++) {
                            GraphUser user = users.get(i);
                            elements.add(new FriendElement(user.getId(), user.getName(), user.getLink()));
                        }
                        Context c = FriendsListActivity.this;
                        FriendAdapter adapter = new FriendAdapter(FriendsListActivity.this, R.layout.listitem, elements);
                        lvFriend.setAdapter(adapter);
                    }
                });
        request.executeAsync();
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            makeFriendsRequest(session);
        }
    }

    private class FriendAdapter extends ArrayAdapter<FriendElement> {
        private List<FriendElement> elements;

        public FriendAdapter(Context context, int resourceId,
                             List<FriendElement> elements) {
            super(context, resourceId, elements);
            this.elements = elements;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater =
                        (LayoutInflater) FriendsListActivity.this
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.listitem, null);
            }

            FriendElement element = elements.get(position);
            if (element != null) {
                ProfilePictureView photo = (ProfilePictureView) view.findViewById(R.id.ppvItem);
                TextView name = (TextView) view.findViewById(R.id.tvItemName);
                TextView link = (TextView) view.findViewById(R.id.tvItemLink);

                photo.setProfileId(element.getId());
                name.setText(element.getName());
                link.setText(element.getLink());
            }
            return view;
        }
    }
}
