package com.facebookapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by taras on 09.02.14.
 */
public class SelectionFragment extends Fragment {
    private static final String TAG = "SelectionFragment";
    private static final int REAUTH_ACTIVITY_CODE = 100;
    private static final int CAMERA_ACTIVITY_CODE = 200;

    private ProfilePictureView profilePictureView;
    private TextView userNameView;
    private TextView userBirthdayView;
    private TextView userLinkView;
    private TextView userGenderView;
    private TextView userHometownView;
    private TextView userLocationView;
    private LinearLayout userEducationView;

    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.selection,
                container, false);

        profilePictureView = (ProfilePictureView) view.findViewById(R.id.selection_profile_pic);
        profilePictureView.setCropped(true);

        userNameView = (TextView) view.findViewById(R.id.selection_user_name);
        userBirthdayView = (TextView) view.findViewById(R.id.selection_user_birthday);
        userLinkView = (TextView) view.findViewById(R.id.selection_user_link);
        userGenderView = (TextView) view.findViewById(R.id.selection_user_gender);
        userHometownView = (TextView) view.findViewById(R.id.selection_user_hometown);
        userLocationView = (TextView) view.findViewById(R.id.selection_user_location);
        userEducationView = (LinearLayout) view.findViewById(R.id.llEducation);

        Button btnFriends = (Button) view.findViewById(R.id.btnFriends);
        btnFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startFriendListActivity();
            }
        });

        Button btnShare = (Button) view.findViewById(R.id.btnSharePhoto);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startShareActivity();
            }
        });

        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // Get the user's data
            makeMeRequest(session);
        }
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REAUTH_ACTIVITY_CODE) {
            uiHelper.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == CAMERA_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Intent intent = new Intent(getActivity(), ShareActivity.class);
                intent.putExtra("path", data.getStringExtra("path"));
                startActivity(intent);
            }
        } else if (resultCode == Activity.RESULT_OK) {
            // Do nothing for now
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        uiHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    private void startFriendListActivity() {

        Intent intent = new Intent(getActivity(), FriendsListActivity.class);
        startActivity(intent);
    }

    private void startShareActivity() {

        Intent intent = new Intent(getActivity(), CameraActivity.class);
        startActivityForResult(intent, CAMERA_ACTIVITY_CODE);
    }

    private void makeMeRequest(final Session session) {
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if (session == Session.getActiveSession()) {
                            if (user != null) {
                                profilePictureView.setProfileId(user.getId());
                                userNameView.setText("Name: " + user.getName());
                                userBirthdayView.setText("Birthday: " + user.getBirthday());
                                userLinkView.setText("Link: " + user.getLink());
                                JSONObject object = user.getInnerJSONObject();
                                try {
                                    userGenderView.setText("Gender: " +
                                            object.getString("gender"));
                                    userHometownView.setText("Hometown: " +
                                            object.getJSONObject("hometown").getString("name"));
                                    userLocationView.setText("Location: " +
                                            object.getJSONObject("location").getString("name"));

                                    JSONArray education = object.getJSONArray("education");
                                    for (int i = 0; i < education.length(); i++) {
                                        JSONObject school = education.getJSONObject(i);

                                        school.getJSONObject("school").getString("name");
                                        TextView tv = new TextView(getActivity());
                                        tv.setText(school.getString("type") + ": " +
                                                school.getJSONObject("school").getString("name"));
                                        userEducationView.addView(tv, ViewGroup.LayoutParams.WRAP_CONTENT,
                                                ViewGroup.LayoutParams.WRAP_CONTENT);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        if (response.getError() != null) {
                        }
                    }
                });
        request.executeAsync();
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            makeMeRequest(session);
        }
    }
}
