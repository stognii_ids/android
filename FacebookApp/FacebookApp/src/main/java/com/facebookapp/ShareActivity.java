package com.facebookapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.Util;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by taras on 09.02.14.
 */
public class ShareActivity extends FragmentActivity {
    private ImageView mPhoto;
    private EditText mName;

    private String path;

    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        mPhoto = (ImageView) findViewById(R.id.ivPhoto);
        mName = (EditText) findViewById(R.id.etName);

        Intent intent = getIntent();
        path = intent.getStringExtra("path");

        mPhoto.setImageBitmap(Utils.decodeBitmapFromFile(path, 100, 100));

        Button button = (Button) findViewById(R.id.btnShare);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session session = Session.getActiveSession();
                if (session != null && session.isOpened()) {
                    // Get the user's data

                    sharePhoto(session);
                }
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        uiHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            //makeFriendsRequest(session);

            sharePhoto(session);
        }
    }

    private void sharePhoto(Session session) {
        File file = new File(path);
        try {

            Request request = Request.newUploadPhotoRequest(session, file, new Request.Callback() {
                @Override
                public void onCompleted(Response response) {
                    Log.d("myLog", "upload");
                }
            });
            Bundle params = request.getParameters();
            params.putString("name", mName.getText().toString());
            request.setParameters(params);
            request.executeAsync();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
