package com.facebookapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.facebook.Request;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by taras on 09.02.14.
 */
public class CameraActivity extends FragmentActivity implements Camera.PictureCallback, View.OnClickListener {
    public static final String TAG = "ParsePhotoApp";

    private static int mCurrentCamera = 0;
    private int mRequestCode;
    ImageButton mCaptureButton;
    private File mOutputFile;

    private boolean mIsRecord = false;

    private MediaRecorder mMediaRecorder;
    private Camera mCamera;
    private CameraPreview mCameraPreview;
    private Facebook mFacebook;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mCaptureButton = (ImageButton) findViewById(R.id.btnCapture);
        mCaptureButton.setOnClickListener(this);

        ImageButton anotherCameraButton = (ImageButton) findViewById(R.id.btnAnotherCamera);
        anotherCameraButton.setOnClickListener(this);

        Button flashButton = (Button) findViewById(R.id.btnFlash);
        flashButton.setOnClickListener(this);

        mCamera = getCameraInstance();
        Camera.Parameters parameters = null;
        if (mCamera == null) {
            Log.d(TAG, "Error");
            finish();
        } else {
            mCameraPreview = new CameraPreview(this, mCamera);
            parameters = mCamera.getParameters();

            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(mCameraPreview);

        }

        if (Camera.getNumberOfCameras() <= 1) {
            anotherCameraButton.setVisibility(View.GONE);
            anotherCameraButton.setEnabled(false);
        }
        if (parameters != null) {
            if (parameters.getSupportedFlashModes() == null) {
                flashButton.setVisibility(View.GONE);
                flashButton.setEnabled(false);
            } else {
                flashButton.setText(parameters.getFlashMode());
            }
        }
        if (!anotherCameraButton.isEnabled() && !flashButton.isEnabled()) {
            LinearLayout llAnotherCamera = (LinearLayout) findViewById(R.id.llAnotherCamera);
            llAnotherCamera.setVisibility(View.GONE);
        }
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(mCurrentCamera);
        } catch (Exception e) {
            Log.d(TAG, "getCameraInstanceError: " + e.getMessage());
        }
        return c;
    }

    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        File pictureFile = Utils.getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG, "Error");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(bytes);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "Error" + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error" + e.getMessage());
        }

        Intent intent = new Intent();
        intent.putExtra("path", pictureFile.getAbsolutePath());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCapture:
                mCamera.takePicture(null, null, this);
                break;
            case R.id.btnAnotherCamera:
                if (mIsRecord)
                    break;
                mCurrentCamera = (mCurrentCamera == 0) ? 1 : 0;

                mCamera = getCameraInstance();
                mCameraPreview = new CameraPreview(this, mCamera);
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                preview.removeAllViews();
                preview.addView(mCameraPreview);
                break;
            case R.id.btnFlash:
                Camera.Parameters parameters = mCamera.getParameters();
                List<String> flashModes = parameters.getSupportedFlashModes();
                int index = flashModes.indexOf(((Button)view).getText());

                String mode = flashModes.get((index == flashModes.size() -1) ? 0 : index + 1);
                parameters.setFlashMode(mode);
                mCamera.setParameters(parameters);
                break;
        }
    }
}
