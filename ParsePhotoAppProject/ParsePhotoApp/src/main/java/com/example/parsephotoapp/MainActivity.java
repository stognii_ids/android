package com.example.parsephotoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.parsephotoapp.database.Database;

/**
 * Created by taras on 03.02.14.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonMakePhoto = (Button) findViewById(R.id.btnMakePhoto);
        Button buttonSelectPhoto = (Button) findViewById(R.id.btnSelectPhoto);
        Button buttonMakeVideo = (Button) findViewById(R.id.btnMakeVideo);
        Button buttonSelectVideo = (Button) findViewById(R.id.btnSelectVideo);
        Button buttonMap = (Button) findViewById(R.id.btnMap);

        buttonMakePhoto.setOnClickListener(this);
        buttonSelectPhoto.setOnClickListener(this);
        buttonMakeVideo.setOnClickListener(this);
        buttonSelectVideo.setOnClickListener(this);
        buttonMap.setOnClickListener(this);





        //mLocationClient = new LocationClient(this, this, this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnMakePhoto:
                intent = new Intent(this, CameraActivity.class);
                intent.putExtra(getString(R.string.field_request_code), ParseObject.TYPE_PHOTO);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, ParseObject.TYPE_PHOTO);
                break;
            case R.id.btnSelectPhoto:
                intent = new Intent(this, TablePhotosActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(getString(R.string.field_type), ParseObject.TYPE_PHOTO);
                startActivity(intent);
                break;
            case R.id.btnMakeVideo:
                intent = new Intent(this, CameraActivity.class);
                intent.putExtra(getString(R.string.field_request_code), ParseObject.TYPE_VIDEO);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, ParseObject.TYPE_VIDEO);
                break;
            case R.id.btnSelectVideo:
                intent = new Intent(this, TablePhotosActivity.class);
                intent.putExtra(getString(R.string.field_type), ParseObject.TYPE_VIDEO);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.btnMap:
                intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        String name;
        String path;
        int type;

        if (data != null && resultCode == RESULT_OK) {
            name = data.getStringExtra(getString(R.string.field_name));
            path = data.getStringExtra(getString(R.string.field_path));
            type = data.getIntExtra(getString(R.string.field_type), 0);

            ParseObject parseObject = new Database(this).addObject(name, path, type);
            if (parseObject != null) {
                Intent intent = new Intent(this, PreviewActivity.class);
                intent.putExtra(getString(R.string.field_path), parseObject.getPath());
                intent.putExtra(getString(R.string.field_id), parseObject.getId());
                intent.putExtra(getString(R.string.field_type), parseObject.getType());
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                Toast.makeText(this, getString(R.string.error_save_object), Toast.LENGTH_SHORT).show();
            }


        }
    }


}