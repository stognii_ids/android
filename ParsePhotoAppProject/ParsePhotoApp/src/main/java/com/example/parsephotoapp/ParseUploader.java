package com.example.parsephotoapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Handler;

import com.example.parsephotoapp.database.Database;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Created by taras on 03.02.14.
 */
public class ParseUploader extends Thread {
    private ParseObject mParseObject;
    private Context mContext;
    private Handler mHandler;
    private Location mLocation;

    private static final String APPLICATION_ID = "5VDQeTHNTgUvnGevKj7Je5JmYbzeO5Vs3bCQHVtt";
    private static final String REST_API_KEY = "Dukx1MgxCn1RjSmqH6tlD4jPbAGGIKscnXO2ohWA";

    public ParseUploader(ParseObject parseObject, Location location, Context context, Handler handler) {
        mParseObject = parseObject;
        mContext = context;
        mHandler = handler;
        mLocation = location;
    }

    public void addObject() {
        try {

            byte[] data;
            if (mParseObject.getType() == ParseObject.TYPE_PHOTO) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Bitmap bitmap = ParseObject.Utils.decodeBitmapFromFile(mParseObject.getPath(), 100, 100);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                data = bos.toByteArray();
            } else {
                File file = new File(mParseObject.getPath());
                int size = (int) file.length();
                data = new byte[size];
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                buf.read(data, 0, data.length);
                buf.close();
            }

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(mContext.getString(R.string.url_files) + mParseObject.getName());
            httpPost.setHeader(mContext.getString(R.string.content_app_id), APPLICATION_ID);
            httpPost.setHeader(mContext.getString(R.string.content_api_key), REST_API_KEY);
            if (mParseObject.getType() == ParseObject.TYPE_PHOTO)
                httpPost.setHeader(mContext.getString(R.string.content_type), mContext.getString(R.string.content_type_image));
            else
                httpPost.setHeader(mContext.getString(R.string.content_type), mContext.getString(R.string.content_type_video));
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(data);

            if (mParseObject.getType() == ParseObject.TYPE_PHOTO)
                byteArrayEntity.setContentType(mContext.getString(R.string.content_type_image));
            else
                byteArrayEntity.setContentType(mContext.getString(R.string.content_type_video));

            httpPost.setEntity(new ByteArrayEntity(data));

            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_CREATED) {
                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }
                br.close();

                JSONObject jsonObject = new JSONObject(stringBuilder.toString());

                String name = jsonObject.getString(mContext.getString(R.string.field_name));

                mParseObject.setParseName(name);
                createObject();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createObject() {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(mContext.getString(R.string.url_classes));
            httpPost.setHeader(mContext.getString(R.string.content_app_id), APPLICATION_ID);
            httpPost.setHeader(mContext.getString(R.string.content_api_key), REST_API_KEY);
            httpPost.setHeader(mContext.getString(R.string.content_type), mContext.getString(R.string.content_type_json));
            JSONObject object = new JSONObject();
            object.put(mContext.getString(R.string.field_name), mParseObject.getName());
            JSONObject parseObject = new JSONObject();
            parseObject.put(mContext.getString(R.string.field_name), mParseObject.getParseName());
            parseObject.put(mContext.getString(R.string.content_type_type), mContext.getString(R.string.content_type_file));
            object.put(mContext.getString(R.string.field_object_col_photo), parseObject);
            StringEntity stringEntity = new StringEntity(object.toString());
            stringEntity.setContentType(mContext.getString(R.string.content_type_json));
            httpPost.setEntity(stringEntity);

            HttpResponse response = httpClient.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_CREATED) {
                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }
                br.close();

                JSONObject jsonObject = new JSONObject(stringBuilder.toString());
                mParseObject.setSharedId(jsonObject.getString(mContext.getString(R.string.field_object_id)));
                if (mLocation != null) {
                    mParseObject.setmLatitude(mLocation.getLatitude());
                    mParseObject.setmLongitude(mLocation.getLongitude());
                }

                new Database(mContext).updateObjects(mParseObject);


            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mHandler.sendEmptyMessage(0);
    }

    @Override
    public void run() {
        addObject();
    }
}
