package com.example.parsephotoapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by taras on 03.02.14.
 */
public class ParseObject {
    public static final int TYPE_PHOTO = 1;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_THUMB = 3;

    private long mId;
    private String mName;
    private String mParseName;
    private String mPath;
    private String mPathToThumb;
    private String mObjectId;
    private int mType;
    private double mLatitude;
    private double mLongitude;

    public ParseObject(long id, String name, String parseName, String path,
                       String pathToThumb, String objectId, int type,
                       double latitude, double longitude) {
        mId = id;
        mName = name;
        mParseName = parseName;
        mPath = path;
        mPathToThumb = pathToThumb;
        mObjectId = objectId;
        mType = type;
        mLatitude = latitude;
        mLongitude = longitude;
    }
    public ParseObject(long id, String name, String path, String pathToThumb, int type) {
        mId = id;
        mName = name;
        mPath = path;
        mPathToThumb = pathToThumb;
        mType = type;
    }

    public String getSharedId() {return mObjectId;}
    public void setSharedId(String mSharedId) {
        this.mObjectId = mSharedId;
    }

    public void setId(int id) {mId = id;}
    public long getId() {return mId;}

    public void setName(String name) {mName = name;}
    public String getName() {return mName;}

    public void setParseName(String parseName) {mParseName = parseName;}
    public String getParseName() {return mParseName;}

    public String getPath() {return mPath;}
    public void setPath(String mPath) {this.mPath = mPath;}

    public String getPathToThumb() {return mPathToThumb;}
    public void setPathToThumb(String mPathToThumb) {this.mPathToThumb = mPathToThumb;}

    public int getType() {return mType;}
    public void setType(int mType) {this.mType = mType;}

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude)
    {

        this.mLatitude = new BigDecimal(mLatitude).setScale(5, RoundingMode.FLOOR).doubleValue();
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = new BigDecimal(mLongitude).setScale(5, RoundingMode.FLOOR).doubleValue();
    }

    public static class Utils {

        public static int calculateInSampleSize(
                BitmapFactory.Options options, int reqWidth, int reqHeight) {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > reqHeight || width > reqWidth) {
                final int halfHeight = height / 2;
                final int halfWidth = width / 2;
                while ((halfHeight / inSampleSize) > reqHeight
                        && (halfWidth / inSampleSize) > reqWidth) {
                    inSampleSize *= 2;
                }
            }

            return inSampleSize;
        }

        public static Bitmap decodeBitmapFromFile(String path, int reqWidth, int reqHeight) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(path, options);
        }

        public static String createVideoThumb(String path) {

            File file = getOutputMediaFile(ParseObject.TYPE_THUMB);
            if (file == null)
                return null;
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(file);

                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MICRO_KIND);//MINI_KIND!!!!
                if (thumb == null)
                    return null;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                thumb.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                fos.write(byteArray);
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return file.getAbsolutePath();
        }

        public static File getOutputMediaFile(int type) {
            File mediaStorageDir;
            String directory;
            String name;

            switch (type) {
                case TYPE_PHOTO:
                case TYPE_THUMB:
                    directory = Environment.DIRECTORY_PICTURES;
                    break;
                case TYPE_VIDEO:
                    directory = Environment.DIRECTORY_MOVIES;
                    break;
                default:
                    directory = "";
            }

            if (type == TYPE_THUMB)
                name = "ParsePhotoApp/Thumb";
            else
                name = "ParsePhotoApp";

            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                Log.d("ParsePhotoApp", "SdCart not mounted");
                return null;
            }

            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(directory), name);

            if (! mediaStorageDir.exists()){
                if (! mediaStorageDir.mkdirs()){
                    Log.d("ParsePhotoApp", "failed to create directory " + name);
                    return null;
                }
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaFile;
            switch (type) {
                case TYPE_PHOTO:
                    mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                            "IMG_"+ timeStamp + ".jpg");
                    break;
                case TYPE_VIDEO:
                    mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                            "VID_"+ timeStamp + ".mp4");
                    break;
                case TYPE_THUMB:
                    mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                            "thumb_"+ timeStamp + ".jpg");
                    break;
                default:
                    return null;
            }

            return mediaFile;
        }
    }
}
