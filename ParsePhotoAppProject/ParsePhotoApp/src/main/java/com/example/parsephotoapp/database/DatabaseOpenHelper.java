package com.example.parsephotoapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by taras on 03.02.14.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public static final String dbTableName = "Objects";
    private static final String dbTableNameOld = "Photos";

    public static final String dbColumnId = "_id";
    public static final String dbColumnName = "name";
    public static final String dbColumnParseName = "parse_name";
    public static final String dbColumnPath = "path";
    public static final String dbColumnPathToThumb = "thumb";
    public static final String dbColumnObjectId = "object_id";
    public static final String dbColumnType = "type";
    public static final String dbColumnLatitude = "latitude"; //-90..90
    public static final String dbColumnLongitude = "longitude"; //-180..180


    public DatabaseOpenHelper(Context context) {
        super(context, dbTableName, null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + dbTableName +
                " (" + dbColumnId + " integer primary key autoincrement, " +
                dbColumnName + " text, " +
                dbColumnParseName + " text, " +
                dbColumnPath + " text, " +
                dbColumnObjectId + " text, " +
                dbColumnType + " integer, " +
                dbColumnPathToThumb + " text, " +
                dbColumnLatitude + " real , " +
                dbColumnLongitude + " real);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + dbTableName + ";");
        onCreate(sqLiteDatabase);
    }
}
