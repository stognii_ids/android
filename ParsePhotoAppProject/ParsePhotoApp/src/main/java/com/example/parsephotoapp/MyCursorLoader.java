package com.example.parsephotoapp;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import com.example.parsephotoapp.database.Database;

/**
 * Created by taras on 04.02.14.
 */
public class MyCursorLoader extends CursorLoader {
    private Context mContext;
    private int mType;

    public MyCursorLoader(Context context, int type) {
        super(context);
        mContext = context;
        mType = type;
    }

    @Override
    public Cursor loadInBackground() {
        Database database = new Database(mContext);
        return database.getObjectsByType(mType);
    }
}
