package com.example.parsephotoapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.parsephotoapp.ParseObject;
import com.google.android.gms.internal.el;

import java.util.ArrayList;

/**
 * Created by taras on 03.02.14.
 */
public class Database {
    private DatabaseOpenHelper mDatabaseOpenHelper;

    public Database(Context context) {
        mDatabaseOpenHelper = new DatabaseOpenHelper(context);
    }

    public ParseObject addObject(String name, String path, int type) {
        long id = -1;
        ContentValues cv = new ContentValues();
        cv.put(DatabaseOpenHelper.dbColumnName, name);
        cv.put(DatabaseOpenHelper.dbColumnParseName, "");
        cv.put(DatabaseOpenHelper.dbColumnPath, path);
        cv.put(DatabaseOpenHelper.dbColumnObjectId, "");
        cv.put(DatabaseOpenHelper.dbColumnType, type);
        cv.put(DatabaseOpenHelper.dbColumnLatitude, -200);
        cv.put(DatabaseOpenHelper.dbColumnLongitude, -200);
        String pathToThumb = "";
        if (type == ParseObject.TYPE_VIDEO)
            pathToThumb = ParseObject.Utils.createVideoThumb(path);
        if (pathToThumb == null)
            return null;
        cv.put(DatabaseOpenHelper.dbColumnPathToThumb, pathToThumb);

        SQLiteDatabase database = mDatabaseOpenHelper.getWritableDatabase();
        if (database == null)
            return null;
        id = database.insert(DatabaseOpenHelper.dbTableName, null, cv);
        database.close();

        return (id != -1) ? new ParseObject(id, name, path, pathToThumb, type) : null;
    }

    public ParseObject getObjectById(long id) {
        SQLiteDatabase database = mDatabaseOpenHelper.getReadableDatabase();
        ParseObject parseObject = null;
        if (database == null)
            return null;
        Cursor c = database.query(DatabaseOpenHelper.dbTableName, null,
                DatabaseOpenHelper.dbColumnId + "=" + id, null, null, null, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
             parseObject = new ParseObject(c.getInt(c.getColumnIndex(DatabaseOpenHelper.dbColumnId)),
                     c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnName)),
                     c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnParseName)),
                     c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnPath)),
                     c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnPathToThumb)),
                     c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnObjectId)),
                     c.getInt(c.getColumnIndex(DatabaseOpenHelper.dbColumnType)),
                     c.getDouble(c.getColumnIndex(DatabaseOpenHelper.dbColumnLatitude)),
                     c.getDouble(c.getColumnIndex(DatabaseOpenHelper.dbColumnLongitude)));
            c.close();
            database.close();
        }
        return parseObject;
    }

    public Cursor getAllObjects() {
        SQLiteDatabase database = mDatabaseOpenHelper.getReadableDatabase();
        if (database == null)
            return null;
        Cursor c = database.query(DatabaseOpenHelper.dbTableName, null, null, null, null, null, null);
        return c;
    }

    public Cursor getObjectsByType(int type) {
        SQLiteDatabase database = mDatabaseOpenHelper.getReadableDatabase();
        if (database == null)
            return null;
        Cursor c = database.query(DatabaseOpenHelper.dbTableName, null, DatabaseOpenHelper.dbColumnType + "=" + type, null, null, null, null);
        return c;
    }

    public int getObjectsCount() {
        SQLiteDatabase database = mDatabaseOpenHelper.getReadableDatabase();
        int count = 0;
        if (database == null)
            return count;
        Cursor c = database.query(DatabaseOpenHelper.dbTableName, null, null, null, null, null, null);
        count = c.getCount();
        c.close();
        database.close();
        return count;
    }

    public String getPathObjectByCoordinates(double latitude, double longitude) {
        String path;
        SQLiteDatabase database = mDatabaseOpenHelper.getReadableDatabase();
        if (database == null)
            return null;
        Cursor c = database.query(DatabaseOpenHelper.dbTableName, null,
                DatabaseOpenHelper.dbColumnLatitude + "=" + latitude + " AND " +
                        DatabaseOpenHelper.dbColumnLongitude + "=" + longitude, null, null, null, null);

        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            if (c.getInt(c.getColumnIndex(DatabaseOpenHelper.dbColumnType)) == ParseObject.TYPE_PHOTO)
                path = c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnPath));
            else
                path = c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnPathToThumb));
            c.close();
            database.close();
        } else
            path = "";
        return path;
    }

    public ArrayList<ParseObject> getAllSharedObjects() {
        ArrayList<ParseObject> objects = new ArrayList<ParseObject>();
        SQLiteDatabase database = mDatabaseOpenHelper.getReadableDatabase();
        if (database == null)
            return null;
        Cursor c = database.query(DatabaseOpenHelper.dbTableName, null, DatabaseOpenHelper.dbColumnObjectId + "<>\'\'", null, null, null, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                String lat = c.getDouble(c.getColumnIndex(DatabaseOpenHelper.dbColumnLatitude)) + "";
                objects.add(new ParseObject(c.getInt(c.getColumnIndex(DatabaseOpenHelper.dbColumnId)),
                            c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnName)),
                            c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnParseName)),
                            c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnPath)),
                            c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnPathToThumb)),
                            c.getString(c.getColumnIndex(DatabaseOpenHelper.dbColumnObjectId)),
                            c.getInt(c.getColumnIndex(DatabaseOpenHelper.dbColumnType)),
                            c.getDouble(c.getColumnIndex(DatabaseOpenHelper.dbColumnLatitude)),
                            c.getDouble(c.getColumnIndex(DatabaseOpenHelper.dbColumnLongitude))));
            } while (c.moveToNext());
        }
        return objects;
    }
    
    public int updateObjects(ParseObject parseObject) {
        int updated = 0;
        SQLiteDatabase database = mDatabaseOpenHelper.getWritableDatabase();
        if (database == null)
            return updated;
        ContentValues cv = new ContentValues();
        cv.put(DatabaseOpenHelper.dbColumnId, parseObject.getId());
        cv.put(DatabaseOpenHelper.dbColumnName, parseObject.getName());
        cv.put(DatabaseOpenHelper.dbColumnPath, parseObject.getPath());
        cv.put(DatabaseOpenHelper.dbColumnPathToThumb, parseObject.getPathToThumb());
        cv.put(DatabaseOpenHelper.dbColumnParseName, parseObject.getParseName());
        cv.put(DatabaseOpenHelper.dbColumnObjectId, parseObject.getSharedId());
        cv.put(DatabaseOpenHelper.dbColumnType, parseObject.getType());
        cv.put(DatabaseOpenHelper.dbColumnLatitude, parseObject.getmLatitude());
        cv.put(DatabaseOpenHelper.dbColumnLongitude, parseObject.getmLongitude());

        updated = database.update(DatabaseOpenHelper.dbTableName, cv, DatabaseOpenHelper.dbColumnId+"="+ parseObject.getId(), null);
        database.close();
        return updated;
    }
}
