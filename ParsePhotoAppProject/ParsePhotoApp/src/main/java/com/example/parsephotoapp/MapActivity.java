package com.example.parsephotoapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parsephotoapp.database.Database;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;


/**
 * Created by taras on 06.02.14.
 */
public class MapActivity extends FragmentActivity implements GoogleMap.InfoWindowAdapter {
    private GoogleMap mMap;
    ArrayList<ParseObject> mObjects;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        mObjects = new Database(this).getAllSharedObjects();
        for (int i = 0; i < mObjects.size(); i++) {
            ParseObject object = mObjects.get(i);
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(object.getmLatitude(), object.getmLongitude()))
                    .draggable(false)
                    .title(object.getName()));

        }
        mMap.setInfoWindowAdapter(this);


        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (mMap != null) {
                mObjects = new Database(this).getAllSharedObjects();
                for (int i = 0; i < mObjects.size(); i++) {
                    ParseObject object = mObjects.get(i);
                    Bitmap bitmap = ParseObject.Utils.decodeBitmapFromFile(object.getPath(), 50, 50);
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(object.getmLatitude(), object.getmLongitude()))
                            .title(object.getName()))
                            .setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));

                }
            }
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = getLayoutInflater().inflate(R.layout.marker_window, null);

        TextView tvLatitude = (TextView) view.findViewById(R.id.tvMarkerLatitude);
        TextView tvLongitude = (TextView) view.findViewById(R.id.tvMarkerLongitude);
        ImageView ivMarker = (ImageView) view.findViewById(R.id.ivMarker);
        LatLng latLng = marker.getPosition();

        double latitude = new BigDecimal(latLng.latitude).setScale(5, RoundingMode.CEILING).doubleValue();
        double longitude = new BigDecimal(latLng.longitude).setScale(5, RoundingMode.CEILING).doubleValue();

        String path = new Database(this).getPathObjectByCoordinates(latitude, longitude);
        Bitmap bitmap = ParseObject.Utils.decodeBitmapFromFile(path, 50, 50);

        tvLatitude.setText(latitude + "");
        tvLongitude.setText(longitude + "");
        ivMarker.setImageBitmap(bitmap);
        return view;
    }
}