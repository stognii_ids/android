package com.example.parsephotoapp;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.parsephotoapp.database.Database;
import com.example.parsephotoapp.database.DatabaseOpenHelper;

/**
 * Created by taras on 03.02.14.
 */
public class TablePhotosActivity extends FragmentActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {
    private int mType;

    SimpleCursorAdapter mAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_table);

        mType = getIntent().getIntExtra(getString(R.string.field_type), 0);

        GridView gridView = (GridView) findViewById(R.id.gvPhotos);
        String[] from;
        if (mType == ParseObject.TYPE_PHOTO)
            from = new String[] {DatabaseOpenHelper.dbColumnPath};
        else
            from = new String[] {DatabaseOpenHelper.dbColumnPathToThumb};
        int[] to  = new int[] {R.id.ivPhotoItem};

        mAdapter = new SimpleCursorAdapter(this, R.layout.item, null, from, to, 0);
        gridView.setAdapter(mAdapter);

        getSupportLoaderManager().initLoader(0, null, this);
        gridView.setOnItemClickListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new MyCursorLoader(this, mType);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, PreviewActivity.class);
        ParseObject parseObject = new Database(this).getObjectById(l);
        intent.putExtra(getString(R.string.field_path), parseObject.getPath());
        intent.putExtra(getString(R.string.field_id), parseObject.getId());
        intent.putExtra(getString(R.string.field_type), parseObject.getType());
        startActivity(intent);
    }
}