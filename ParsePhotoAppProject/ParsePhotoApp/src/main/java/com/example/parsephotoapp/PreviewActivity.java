package com.example.parsephotoapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.parsephotoapp.database.Database;

import java.io.File;

/**
 * Created by taras on 03.02.14.
 */
public class PreviewActivity extends Activity implements View.OnClickListener, LocationListener {
    private long mObjectId;
    private ProgressDialog mProgressDialog;
    LocationManager mLocationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        Button btnShare = (Button) findViewById(R.id.btnSharePhoto);
        btnShare.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ImageView imageView = (ImageView) findViewById(R.id.ivPhoto);
            VideoView videoView = (VideoView) findViewById(R.id.vvVideo);
            File file = new  File(extras.getString("path"));
            mObjectId = extras.getLong("id");
            String parseName = new Database(this).getObjectById(mObjectId).getParseName();
            if (parseName != null && !parseName.isEmpty())
                btnShare.setEnabled(false);
            if (extras.getInt("type") == ParseObject.TYPE_PHOTO) {
                videoView.setEnabled(false);
                videoView.setVisibility(View.GONE);
                if(file.exists()){
                    imageView.setImageBitmap(ParseObject.Utils.decodeBitmapFromFile(extras.getString("path"), 200, 200));
                }
            } else {
                imageView.setEnabled(false);
                imageView.setVisibility(View.GONE);
                if(file.exists()){
                    videoView.setVideoPath(extras.getString("path"));
                }
            }
        }
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        onLocationChanged(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
    }

    @Override
    public void onClick(View view) {
        mProgressDialog = ProgressDialog.show(this, getString(R.string.uploading_dialog_title), getString(R.string.uploading_dialog_message));
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                mProgressDialog.dismiss();
                return false;
            }
        });

        view.setEnabled(false);
        ParseObject parseObject = new Database(this).getObjectById(mObjectId);
        Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        ParseUploader parseUploader = new ParseUploader(parseObject, location, this, handler);
        parseUploader.start();


    }

    @Override
    public void onLocationChanged(Location location) {

        /*double latitude = (double) (location.getLatitude());
        double longitude = (double) (location.getLongitude());
        Toast.makeText(this, "Location: " + latitude + " " + longitude, Toast.LENGTH_LONG).show();
        Log.d("myLog", "Location: " + latitude + " " + longitude);*/
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Toast.makeText(this, getString(R.string.status_changed) + s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(this, getString(R.string.provider_enabled) + s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(this, getString(R.string.provider_disabled) + s, Toast.LENGTH_SHORT).show();
    }

}