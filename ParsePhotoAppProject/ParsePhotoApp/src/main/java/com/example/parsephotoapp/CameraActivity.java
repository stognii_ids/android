package com.example.parsephotoapp;

import android.content.Intent;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by taras on 05.02.14.
 */
public class CameraActivity extends ActionBarActivity implements Camera.PictureCallback, View.OnClickListener {
    public static final String TAG = "ParsePhotoApp";

    private static int mCurrentCamera = 0;
    private int mRequestCode;
    ImageButton mCaptureButton;
    private File mOutputFile;

    private boolean mIsRecord = false;

    private MediaRecorder mMediaRecorder;
    private Camera mCamera;
    private CameraPreview mCameraPreview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        if (getIntent() != null) {
            mRequestCode = getIntent().getIntExtra(getString(R.string.field_request_code), 0);
            getIntent().setData(null);
            setIntent(null);
        }

        if (mRequestCode == 0) {
            Log.d(TAG, getString(R.string.error_intent));
            finish();
        }

        getSupportActionBar().hide();

        mCaptureButton = (ImageButton) findViewById(R.id.btnCapture);
        mCaptureButton.setOnClickListener(this);

        ImageButton anotherCameraButton = (ImageButton) findViewById(R.id.btnAnotherCamera);
        anotherCameraButton.setOnClickListener(this);

        Button flashButton = (Button) findViewById(R.id.btnFlash);
        flashButton.setOnClickListener(this);

        mCamera = getCameraInstance();
        Camera.Parameters parameters = null;
        if (mCamera == null) {
            Log.d(TAG, getString(R.string.camera_error));
            finish();
        } else {
            mCameraPreview = new CameraPreview(this, mCamera);
            parameters = mCamera.getParameters();

            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(mCameraPreview);

        }

        if (Camera.getNumberOfCameras() <= 1) {
            anotherCameraButton.setVisibility(View.GONE);
            anotherCameraButton.setEnabled(false);
        }
        if (parameters != null) {
            if (parameters.getSupportedFlashModes() == null) {
                flashButton.setVisibility(View.GONE);
                flashButton.setEnabled(false);
            } else {
                flashButton.setText(parameters.getFlashMode());
            }
        }
        if (!anotherCameraButton.isEnabled() && !flashButton.isEnabled()) {
            LinearLayout llAnotherCamera = (LinearLayout) findViewById(R.id.llAnotherCamera);
            llAnotherCamera.setVisibility(View.GONE);
        }
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(mCurrentCamera);
        } catch (Exception e) {
            Log.d(TAG, "getCameraInstanceError: " + e.getMessage());
        }
        return c;
    }

    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        File pictureFile = ParseObject.Utils.getOutputMediaFile(ParseObject.TYPE_PHOTO);
        if (pictureFile == null) {
            Log.d(TAG, getString(R.string.error_creating_file));
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(bytes);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, getString(R.string.error_file_not_found) + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, getString(R.string.error_accessing_file) + e.getMessage());
        }
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.field_name), pictureFile.getName());
        intent.putExtra(getString(R.string.field_path), pictureFile.getAbsolutePath());
        intent.putExtra(getString(R.string.field_type), ParseObject.TYPE_PHOTO);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCapture:
                switch (mRequestCode) {
                    case ParseObject.TYPE_PHOTO:
                        mCamera.takePicture(null, null, this);
                        break;
                    case ParseObject.TYPE_VIDEO:
                        if (!mIsRecord) {
                            initRecorder();
                            mMediaRecorder.start();
                            mIsRecord = !mIsRecord;
                        } else {
                            try {
                                mMediaRecorder.stop();
                            } catch(RuntimeException e) {
                                Log.d(TAG, getString(R.string.camera_error) + e.getMessage());
                            } finally {
                                mMediaRecorder.reset();
                                mMediaRecorder.release();
                                mCamera.lock();
                                mCamera.stopPreview();
                                mIsRecord = !mIsRecord;

                                Intent intent = new Intent();
                                intent.putExtra(getString(R.string.field_name), mOutputFile.getName());
                                intent.putExtra(getString(R.string.field_path), mOutputFile.getAbsolutePath());
                                intent.putExtra(getString(R.string.field_type), ParseObject.TYPE_VIDEO);
                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                        break;
                    default:
                        break;
                }

                break;
            case R.id.btnAnotherCamera:
                if (mIsRecord)
                    break;
                mCurrentCamera = (mCurrentCamera == 0) ? 1 : 0;

                mCamera = getCameraInstance();
                mCameraPreview = new CameraPreview(this, mCamera);
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                preview.removeAllViews();
                preview.addView(mCameraPreview);
                break;
            case R.id.btnFlash:
                Camera.Parameters parameters = mCamera.getParameters();
                List<String> flashModes = parameters.getSupportedFlashModes();
                int index = flashModes.indexOf(((Button)view).getText());

                String mode = flashModes.get((index == flashModes.size() -1) ? 0 : index + 1);
                parameters.setFlashMode(mode);
                mCamera.setParameters(parameters);
                break;
        }
    }

    public boolean initRecorder() {

        mMediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {

            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);

        } else {
            mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
            mMediaRecorder.setVideoSize(720, 480);
        }

        mOutputFile = ParseObject.Utils.getOutputMediaFile(ParseObject.TYPE_VIDEO);
        mMediaRecorder.setOutputFile(mOutputFile.getAbsolutePath());
        mMediaRecorder.setPreviewDisplay(mCameraPreview.getHolder().getSurface());

        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, getString(R.string.error_preparing_recoder) + e.getMessage());
            mMediaRecorder.release();
            return false;
        } catch (IOException e) {
            Log.d(TAG, getString(R.string.error_preparing_recoder) + e.getMessage());
            mMediaRecorder.release();
            return false;
        }
        return true;
    }
}