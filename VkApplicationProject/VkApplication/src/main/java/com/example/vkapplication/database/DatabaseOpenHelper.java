package com.example.vkapplication.database;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vkapplication.R;

/**
 * Created by taras on 29.01.14.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {
    private static final String tableName = "Friends";

    private final String columnId = "_id";
    private final String columnUid = "uid";
    private final String columnFirstName = "first_name";
    private final String columnLastName = "last_name";
    private final String columnSex = "sex";
    private final String columnBirthday = "birthday";
    private final String columnCity = "city";
    private final String columnPhoto = "photo";
    private final String columnPhotoMax = "photo_max";

    public DatabaseOpenHelper(Context context) {
        super(context, tableName, null, 5);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + tableName +
            " (" + columnId + " integer primary key autoincrement, " +
                columnUid + " integer, " +
                columnFirstName + " text, " +
                columnLastName + " text, " +
                columnSex + " integer, " +
                columnBirthday + " text, " +
                columnCity + " text, " +
                columnPhoto + " text, " +
                columnPhotoMax + " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + tableName);
        onCreate(sqLiteDatabase);
    }
}
