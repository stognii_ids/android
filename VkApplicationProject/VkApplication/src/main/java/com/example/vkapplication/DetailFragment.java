package com.example.vkapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vkapplication.database.Database;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by taras on 29.01.14.
 */
public class DetailFragment extends Fragment {
    private int mIndex = -1;
    private ImageLoader mImageLoader;

    public DetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mIndex = getArguments().getInt("index");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_fragment, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.ivPhoto);
        TextView tvFirstName = (TextView) view.findViewById(R.id.tvFirstName);
        TextView tvLastName = (TextView) view.findViewById(R.id.tvLastName);
        TextView tvSex = (TextView) view.findViewById(R.id.tvSex);
        TextView tvBirthday = (TextView) view.findViewById(R.id.tvBirthday);
        TextView tvCity = (TextView) view.findViewById(R.id.tvCity);

        if (getShownIndex() != -1) {
            Database database = new Database(getActivity());
            Friend friend = database.getFriendById(getShownIndex());
            if (friend != null) {
                tvFirstName.setText(friend.getFirstName());
                tvLastName.setText(friend.getmLastName());
                if (friend.getSex() == 1)
                    tvSex.setText("female");
                else
                    tvSex.setText("male");
                tvBirthday.setText(friend.getBirthday());
                tvCity.setText(friend.getCity());

                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .showImageOnLoading(R.drawable.ic_launcher)
                        .showImageForEmptyUri(R.drawable.ic_launcher)
                        .resetViewBeforeLoading(true)
                        .cacheInMemory(true)
                        .cacheOnDisc(true)
                        .imageScaleType(ImageScaleType.EXACTLY)
                        .build();
                ImageLoader.getInstance().displayImage(friend.getMaxPhoto(), imageView, options);
            }
        }
        return view;
    }

    public static DetailFragment newInstance(int index) {
        DetailFragment detailFragment = new DetailFragment();

        Bundle args = new Bundle();
        args.putInt("index", index);
        detailFragment.setArguments(args);
        detailFragment.setRetainInstance(true);

        return detailFragment;
    }

    public int getShownIndex() {
        return mIndex;
    }
}
