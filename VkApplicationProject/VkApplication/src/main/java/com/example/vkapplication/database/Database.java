package com.example.vkapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.vkapplication.Friend;
import com.example.vkapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taras on 29.01.14.
 */
public class Database  {
    private DatabaseOpenHelper mDatabaseOpenHelper;

    private final String tableName = "Friends";

    private final String columnId = "_id";
    private final String columnUid = "uid";
    private final String columnFirstName = "first_name";
    private final String columnLastName = "last_name";
    private final String columnSex = "sex";
    private final String columnBirthday = "birthday";
    private final String columnCity = "city";
    private final String columnPhoto = "photo";
    private final String columnPhotoMax = "photo_max";

    //ArrayList<Friend> mFriends;

    public Database(Context context) {
        mDatabaseOpenHelper = new DatabaseOpenHelper(context);
        //mFriends = new ArrayList<Friend>();
    }

    public void addFriend(Friend friend) {
        ContentValues cv = new ContentValues();
        cv.put(columnUid, friend.getId());
        cv.put(columnFirstName, friend.getFirstName());
        cv.put(columnLastName, friend.getmLastName());
        cv.put(columnSex, friend.getSex());
        cv.put(columnBirthday, friend.getBirthday());
        cv.put(columnCity, friend.getCity());
        cv.put(columnPhoto, friend.getPhoto());
        cv.put(columnPhotoMax, friend.getMaxPhoto());

        SQLiteDatabase sqLiteDatabase = mDatabaseOpenHelper.getWritableDatabase();
        sqLiteDatabase.beginTransaction();
        try {
            sqLiteDatabase.insert(tableName, null, cv);
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    public void addFriend(String uid, String firstName, String lastName, int sex, String birthday, String city, String photo, String photoMax) {
        ContentValues cv = new ContentValues();
        cv.put(columnUid, uid);
        cv.put(columnFirstName, firstName);
        cv.put(columnLastName, lastName);
        cv.put(columnSex, sex);
        cv.put(columnBirthday, birthday);
        cv.put(columnCity, city);
        cv.put(columnPhoto, photo);
        cv.put(columnPhotoMax, photoMax);

        SQLiteDatabase sqLiteDatabase = mDatabaseOpenHelper.getWritableDatabase();
        sqLiteDatabase.beginTransaction();
        try {
            sqLiteDatabase.insert(tableName, null, cv);
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    public ArrayList<Friend> getAllFriends() {
        SQLiteDatabase sqLiteDatabase = mDatabaseOpenHelper.getWritableDatabase();
        Cursor result = sqLiteDatabase.query(tableName, null, null, null, null, null, null);

        ArrayList<Friend> friends = new ArrayList<Friend>();
        if (result.getCount() == 0)
            return null;
        result.moveToFirst();
        do {
            Friend friend = new Friend();
            friend.setId(result.getInt(result.getColumnIndex(columnId)));
            friend.setFirstName(result.getString(result.getColumnIndex(columnFirstName)));
            friend.setLastName(result.getString(result.getColumnIndex(columnLastName)));
            friend.setSex(result.getInt(result.getColumnIndex(columnSex)));
            friend.setBirthday(result.getString(result.getColumnIndex(columnBirthday)));
            friend.setCity(result.getString(result.getColumnIndex(columnCity)));
            friend.setPhoto(result.getString(result.getColumnIndex(columnPhoto)));
            friend.setMaxPhoto(result.getString(result.getColumnIndex(columnPhotoMax)));

            friends.add(friend);
        } while (result.moveToNext());

        return friends;
    }

    public Friend getFriendById(int id) {
        SQLiteDatabase sqLiteDatabase = mDatabaseOpenHelper.getReadableDatabase();
        Cursor result = sqLiteDatabase.query(tableName, null, "_id=" + id, null, null, null, null);

        Friend friend;
        if (result.getCount() == 0)
            friend = null;
        else {
            result.moveToFirst();
            friend = new Friend();
            friend.setId(result.getInt(result.getColumnIndex(columnId)));
            friend.setFirstName(result.getString(result.getColumnIndex(columnFirstName)));
            friend.setLastName(result.getString(result.getColumnIndex(columnLastName)));
            friend.setSex(result.getInt(result.getColumnIndex(columnSex)));
            friend.setBirthday(result.getString(result.getColumnIndex(columnBirthday)));
            friend.setCity(result.getString(result.getColumnIndex(columnCity)));
            friend.setPhoto(result.getString(result.getColumnIndex(columnPhoto)));
            friend.setMaxPhoto(result.getString(result.getColumnIndex(columnPhotoMax)));
        }
        return friend;
    }

    public ArrayList<Friend> getFriendList() {
        return getAllFriends();
    }

    public int deleteAll() {
        int count;
        SQLiteDatabase sqLiteDatabase = mDatabaseOpenHelper.getWritableDatabase();
        sqLiteDatabase.beginTransaction();
        try {
            count = sqLiteDatabase.delete(tableName, "1", null);
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
        return count;
    }
}
