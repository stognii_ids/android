package com.example.vkapplication;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.vkapplication.database.Database;

public class MainActivity extends ActionBarActivity implements WebViewFragment.OnChangeUrlListener {
    public static final String PREFS_NAME = "VkApplicationPrefs";

    WebViewFragment mWebViewFragment;
    String mAccessToken;
    int mUserId, mExpiresIn ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String accessToken = preferences.getString("accessToken", "");
        if (accessToken == "") {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            mWebViewFragment = new WebViewFragment(this, 4151231);
            if (getResources().getConfiguration().orientation
                    == Configuration.ORIENTATION_LANDSCAPE) {
                FriendsFragment friendsFragment = new FriendsFragment();
                fragmentTransaction
                        .add(R.id.list, friendsFragment, "List");
                fragmentTransaction
                        .add(R.id.viewer1, mWebViewFragment, "Web")
                        .commit();
            } else {
                fragmentTransaction
                        .add(R.id.list, mWebViewFragment, "Web")
                        .commit();
            }
        } else {
            FriendsFragment friendsFragment = new FriendsFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.list, friendsFragment, "List")
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onChangeUrl(String url) {
        if (url.startsWith("https://oauth.vk.com/blank.html")) {

            String[] separated = url.split("#")[1].split("&");
            for (int i = 0; i < separated.length; i++) {
                if (separated[i].contains("access_token"))
                    mAccessToken = separated[i].split("=")[1];
                else if (separated[i].contains("expires_in"))
                    mExpiresIn = Integer.parseInt(separated[i].split("=")[1]);
                else
                    mUserId = Integer.parseInt(separated[1].split("=")[1]);
            }

            SharedPreferences preferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("accessToken", mAccessToken);
            editor.commit();

            new Database(this).deleteAll();

            if (getResources().getConfiguration().orientation
                    == Configuration.ORIENTATION_LANDSCAPE) {
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("Web")).commit();
            } else {
                FriendsFragment friendsFragment = new FriendsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.list, friendsFragment, "List").commit();
            }
            mWebViewFragment = null;
        }
    }

}
