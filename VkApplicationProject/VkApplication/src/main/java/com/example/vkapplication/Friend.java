package com.example.vkapplication;

import android.graphics.Bitmap;

/**
 * Created by taras on 29.01.14.
 */
public class Friend {
    private int mId, mSex;
    private String mFirstName, mLastName, mBirthday, mCity, mPhoto, mMaxPhoto;
    private Bitmap mBitmap, mMaxBitmap;

    public Friend() {

    }

    public Friend(int id, String firstName, String lastName, int sex, String birthday, String city, String photo) {
        mId = id;
        mFirstName = firstName;
        mLastName = lastName;
        mSex = sex;
        mBirthday = birthday;
        mCity = city;
        mPhoto = photo;
    }

    public void setId(int id) {mId = id;}
    public int getId() {return mId;}

    public void setFirstName(String firstName) {mFirstName = firstName;}
    public String getFirstName() {return mFirstName;}

    public void setLastName(String lastName) {mLastName = lastName;}
    public String getmLastName() {return  mLastName;}

    public void setSex(int sex) {mSex = sex;}
    public int getSex() {return mSex;}

    public void setBirthday(String birthday) {mBirthday = birthday;}
    public String getBirthday() {return mBirthday;}

    public void setCity(String city) {mCity = city;}
    public String getCity() {return mCity;}

    public void setPhoto(String photo) {mPhoto = photo;}
    public String getPhoto() {return mPhoto;}

    public void setMaxPhoto(String photo) {mMaxPhoto = photo;}
    public String getMaxPhoto() {return mMaxPhoto;}

    public void setBitmap(Bitmap bitmap) {mBitmap = bitmap;}
    public Bitmap getBitmap() {return mBitmap;};

    public void setMaxBitmap(Bitmap bitmap) {mMaxBitmap = bitmap;}
    public Bitmap getMaxBitmap() {return mMaxBitmap;};


}
