package com.example.vkapplication;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by taras on 29.01.14.
 */
public class WebViewFragment extends Fragment {
    private Resources mResources;
    private int mClientId;

    private OnChangeUrlListener mListener;

    public WebViewFragment() {
    }

    public WebViewFragment(Context context, int clientId) {
        mResources = context.getResources();
        mClientId = clientId;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnChangeUrlListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                "must implement OnChangeUrlListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, null, false);
        WebView webView = (WebView)view.findViewById(R.id.wbAuth);
        webView.loadUrl(mResources.getString(R.string.url_auth) +
                mResources.getString(R.string.url_client_id) + mClientId + "&" +
                mResources.getString(R.string.url_redirect_uri) + "&" +
                mResources.getString(R.string.url_display) + "&" +
                mResources.getString(R.string.url_scope) + "&" +
                mResources.getString(R.string.url_response_type));
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mListener.onChangeUrl(url);
            }
        });


        return view;
    }

    public interface OnChangeUrlListener {
        public void onChangeUrl(String url);
    }

}
