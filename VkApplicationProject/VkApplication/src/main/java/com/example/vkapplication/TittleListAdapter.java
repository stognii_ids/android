package com.example.vkapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taras on 30.01.14.
 */

public class TittleListAdapter extends ArrayAdapter<Friend> {
    private Context mContext;
    private ArrayList<Friend> mFriends;

    private static class ViewHolder {
        private ImageView mImageView;
        private TextView mTextView;
        private ImageLoader mImageLoader;

        public void setImageView(ImageView imageView) {mImageView = imageView;}
        public ImageView getImageView() {return  mImageView;}

        public void setTextView(TextView textView) {mTextView = textView;}
        public TextView getTextView() {return mTextView;}

        public void setImageLoader(ImageLoader imageLoader) {mImageLoader = imageLoader;}
        public ImageLoader getImageLoader() {return mImageLoader;}
    }

    public TittleListAdapter(Context context, ArrayList<Friend> friends) {
        super(context, R.layout.list_item);
        mContext = context;
        mFriends = friends;
    }

    @Override
    public int getCount() {
        return mFriends.size();
    }

    @Override
    public Friend getItem(int position) {
        return mFriends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mFriends.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            row = inflater.inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.setImageView((ImageView)row.findViewById(R.id.ivListPhoto));
            viewHolder.setTextView((TextView)row.findViewById(R.id.tvTitle));
            row.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)row.getTag();
        }

        viewHolder.getTextView().setText(mFriends.get(position).getFirstName() + " " + mFriends.get(position).getmLastName());
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();
        ImageLoader.getInstance().displayImage(getItem(position).getPhoto(), viewHolder.getImageView(), options);

        return row;
    }




}