package com.example.vkapplication;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.vkapplication.database.Database;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by taras on 29.01.14.
 */
public class FriendsFragment extends Fragment {
    private ListView mListView;
    private String mAccessToken;


    public FriendsFragment() {
    }

    public void setAdapter(ArrayAdapter<Friend> adapter) {
        mListView.setAdapter(adapter);
    }

    public void setAccessToken(String accessToken) {mAccessToken = accessToken;}

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friends_fragment, container, false);
        mListView = (ListView)view.findViewById(R.id.lvFriends);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showDetails(i);
            }
        });
        new VkToken(mAccessToken, this).getFriendList();
        return view;
    }

    private void showDetails(int index) {
        DetailFragment details = (DetailFragment) getFragmentManager().findFragmentByTag("Details");
        index = (int)mListView.getAdapter().getItemId(index);
        if (details == null || details.getShownIndex() != index) {
            if (getResources().getConfiguration().orientation
                    == Configuration.ORIENTATION_LANDSCAPE) {
                details = DetailFragment.newInstance(index);
                FragmentTransaction ft = getFragmentManager().beginTransaction().replace(R.id.viewer1, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            } else {
                Intent intent = new Intent();
                intent.setClass(getActivity(), DetailActivity.class);
                intent.putExtra("index", index);
                startActivity(intent);
            }
        }
    }

    private class VkToken extends AsyncTask<String, Void, Void> {
        String mAccessToken;
        FriendsFragment mFriendsFragment;

        public VkToken(String accessToken, FriendsFragment friendsFragment) {
            mAccessToken = accessToken;
            mFriendsFragment = friendsFragment;
        }

        public void getFriendList() {
            ArrayList<Friend> friends = new Database(getActivity()).getAllFriends();
            if (friends == null) {
                updateFriendList();
            } else {
                mFriendsFragment.setAdapter(new TittleListAdapter(getActivity(), friends));
            }
        }

        public int updateFriendList() {
            new Database(getActivity()).deleteAll();

            String url = getActivity().getResources().getString(R.string.url_method) +
                    getActivity().getResources().getString(R.string.url_method_friends_get) +
                    getActivity().getResources().getString(R.string.url_fields) + "&" +
                    getActivity().getResources().getString(R.string.url_access_token) + mAccessToken;
            execute(url);
            return 0;
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(strings[0]).openConnection();
                connection.setRequestMethod("GET");
                int status = connection.getResponseCode();

                if (status == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    br.close();

                    JSONObject jsonObject = new JSONObject(stringBuilder.toString());
                    JSONArray jsonArray = jsonObject.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonFriend = jsonArray.getJSONObject(i);
                        Friend friend = new Friend();
                        friend.setId(jsonFriend.getInt("uid"));
                        friend.setFirstName(jsonFriend.getString("first_name"));
                        friend.setLastName(jsonFriend.getString("last_name"));
                        if (jsonFriend.has("sex"))
                            friend.setSex(jsonFriend.getInt("sex"));
                        if (jsonFriend.has("bday"))
                            friend.setBirthday(jsonFriend.getString("bdate"));
                        if (jsonFriend.has("city"))
                            friend.setCity(jsonFriend.getString("city"));
                        if (jsonFriend.has("photo"))
                            friend.setPhoto(jsonFriend.getString("photo"));
                        if (jsonFriend.has("photo_max"))
                            friend.setMaxPhoto(jsonFriend.getString("photo_max"));
                        new Database(getActivity()).addFriend(friend);
                    }
                }

            } catch (MalformedURLException e) {
                Toast.makeText(getActivity(), "Error url: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(getActivity(), "Error io: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(getActivity(), "Error json: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            getFriendList();

        }
    }
}
