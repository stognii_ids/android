package com.example.vkapplication;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by taras on 31.01.14.
 */
public class DetailActivity extends ActionBarActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        DetailFragment details = new DetailFragment();
        details.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().add(R.id.content, details, "Detail").commit();
    }
}
